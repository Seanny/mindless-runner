﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformGenerator
struct  PlatformGenerator_t4213597056  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject PlatformGenerator::thePlatform
	GameObject_t4012695102 * ___thePlatform_2;
	// UnityEngine.GameObject PlatformGenerator::anotherPlatform
	GameObject_t4012695102 * ___anotherPlatform_3;
	// UnityEngine.Transform PlatformGenerator::generationPoint
	Transform_t284553113 * ___generationPoint_4;
	// System.Single PlatformGenerator::platformWidth
	float ___platformWidth_5;
	// System.Single PlatformGenerator::platformWidth2
	float ___platformWidth2_6;
	// System.Int32 PlatformGenerator::oldPlatform
	int32_t ___oldPlatform_7;

public:
	inline static int32_t get_offset_of_thePlatform_2() { return static_cast<int32_t>(offsetof(PlatformGenerator_t4213597056, ___thePlatform_2)); }
	inline GameObject_t4012695102 * get_thePlatform_2() const { return ___thePlatform_2; }
	inline GameObject_t4012695102 ** get_address_of_thePlatform_2() { return &___thePlatform_2; }
	inline void set_thePlatform_2(GameObject_t4012695102 * value)
	{
		___thePlatform_2 = value;
		Il2CppCodeGenWriteBarrier(&___thePlatform_2, value);
	}

	inline static int32_t get_offset_of_anotherPlatform_3() { return static_cast<int32_t>(offsetof(PlatformGenerator_t4213597056, ___anotherPlatform_3)); }
	inline GameObject_t4012695102 * get_anotherPlatform_3() const { return ___anotherPlatform_3; }
	inline GameObject_t4012695102 ** get_address_of_anotherPlatform_3() { return &___anotherPlatform_3; }
	inline void set_anotherPlatform_3(GameObject_t4012695102 * value)
	{
		___anotherPlatform_3 = value;
		Il2CppCodeGenWriteBarrier(&___anotherPlatform_3, value);
	}

	inline static int32_t get_offset_of_generationPoint_4() { return static_cast<int32_t>(offsetof(PlatformGenerator_t4213597056, ___generationPoint_4)); }
	inline Transform_t284553113 * get_generationPoint_4() const { return ___generationPoint_4; }
	inline Transform_t284553113 ** get_address_of_generationPoint_4() { return &___generationPoint_4; }
	inline void set_generationPoint_4(Transform_t284553113 * value)
	{
		___generationPoint_4 = value;
		Il2CppCodeGenWriteBarrier(&___generationPoint_4, value);
	}

	inline static int32_t get_offset_of_platformWidth_5() { return static_cast<int32_t>(offsetof(PlatformGenerator_t4213597056, ___platformWidth_5)); }
	inline float get_platformWidth_5() const { return ___platformWidth_5; }
	inline float* get_address_of_platformWidth_5() { return &___platformWidth_5; }
	inline void set_platformWidth_5(float value)
	{
		___platformWidth_5 = value;
	}

	inline static int32_t get_offset_of_platformWidth2_6() { return static_cast<int32_t>(offsetof(PlatformGenerator_t4213597056, ___platformWidth2_6)); }
	inline float get_platformWidth2_6() const { return ___platformWidth2_6; }
	inline float* get_address_of_platformWidth2_6() { return &___platformWidth2_6; }
	inline void set_platformWidth2_6(float value)
	{
		___platformWidth2_6 = value;
	}

	inline static int32_t get_offset_of_oldPlatform_7() { return static_cast<int32_t>(offsetof(PlatformGenerator_t4213597056, ___oldPlatform_7)); }
	inline int32_t get_oldPlatform_7() const { return ___oldPlatform_7; }
	inline int32_t* get_address_of_oldPlatform_7() { return &___oldPlatform_7; }
	inline void set_oldPlatform_7(int32_t value)
	{
		___oldPlatform_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
