﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// UnityEngine.Animation
struct Animation_t350396337;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1006925219;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t2866526589  : public MonoBehaviour_t3012272455
{
public:
	// System.Single PlayerController::moveSpeed
	float ___moveSpeed_2;
	// System.Single PlayerController::jumpForce
	float ___jumpForce_3;
	// UnityEngine.Rigidbody PlayerController::myRigidbody
	Rigidbody_t1972007546 * ___myRigidbody_4;
	// System.Boolean PlayerController::grounded
	bool ___grounded_5;
	// UnityEngine.LayerMask PlayerController::whatIsGround
	LayerMask_t1862190090  ___whatIsGround_6;
	// System.Boolean PlayerController::isWaiting
	bool ___isWaiting_7;
	// UnityEngine.Animation PlayerController::_anim
	Animation_t350396337 * ____anim_8;
	// UnityEngine.Vector3 PlayerController::oldX
	Vector3_t3525329789  ___oldX_9;
	// System.String PlayerController::systemLabel
	String_t* ___systemLabel_10;
	// System.String PlayerController::positionLabel
	String_t* ___positionLabel_11;
	// System.String PlayerController::gpuLabel
	String_t* ___gpuLabel_12;
	// System.String PlayerController::rigidLabel
	String_t* ___rigidLabel_13;
	// System.String PlayerController::cpuLabel
	String_t* ___cpuLabel_14;
	// UnityEngine.GUIStyle PlayerController::style
	GUIStyle_t1006925219 * ___style_15;
	// System.Int32 PlayerController::scoreValue
	int32_t ___scoreValue_16;
	// System.Int32 PlayerController::highScore
	int32_t ___highScore_17;
	// System.Int32 PlayerController::currentLives
	int32_t ___currentLives_18;
	// System.Single PlayerController::lastUpdate
	float ___lastUpdate_19;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_jumpForce_3() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___jumpForce_3)); }
	inline float get_jumpForce_3() const { return ___jumpForce_3; }
	inline float* get_address_of_jumpForce_3() { return &___jumpForce_3; }
	inline void set_jumpForce_3(float value)
	{
		___jumpForce_3 = value;
	}

	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___myRigidbody_4)); }
	inline Rigidbody_t1972007546 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_t1972007546 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_t1972007546 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier(&___myRigidbody_4, value);
	}

	inline static int32_t get_offset_of_grounded_5() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___grounded_5)); }
	inline bool get_grounded_5() const { return ___grounded_5; }
	inline bool* get_address_of_grounded_5() { return &___grounded_5; }
	inline void set_grounded_5(bool value)
	{
		___grounded_5 = value;
	}

	inline static int32_t get_offset_of_whatIsGround_6() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___whatIsGround_6)); }
	inline LayerMask_t1862190090  get_whatIsGround_6() const { return ___whatIsGround_6; }
	inline LayerMask_t1862190090 * get_address_of_whatIsGround_6() { return &___whatIsGround_6; }
	inline void set_whatIsGround_6(LayerMask_t1862190090  value)
	{
		___whatIsGround_6 = value;
	}

	inline static int32_t get_offset_of_isWaiting_7() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___isWaiting_7)); }
	inline bool get_isWaiting_7() const { return ___isWaiting_7; }
	inline bool* get_address_of_isWaiting_7() { return &___isWaiting_7; }
	inline void set_isWaiting_7(bool value)
	{
		___isWaiting_7 = value;
	}

	inline static int32_t get_offset_of__anim_8() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ____anim_8)); }
	inline Animation_t350396337 * get__anim_8() const { return ____anim_8; }
	inline Animation_t350396337 ** get_address_of__anim_8() { return &____anim_8; }
	inline void set__anim_8(Animation_t350396337 * value)
	{
		____anim_8 = value;
		Il2CppCodeGenWriteBarrier(&____anim_8, value);
	}

	inline static int32_t get_offset_of_oldX_9() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___oldX_9)); }
	inline Vector3_t3525329789  get_oldX_9() const { return ___oldX_9; }
	inline Vector3_t3525329789 * get_address_of_oldX_9() { return &___oldX_9; }
	inline void set_oldX_9(Vector3_t3525329789  value)
	{
		___oldX_9 = value;
	}

	inline static int32_t get_offset_of_systemLabel_10() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___systemLabel_10)); }
	inline String_t* get_systemLabel_10() const { return ___systemLabel_10; }
	inline String_t** get_address_of_systemLabel_10() { return &___systemLabel_10; }
	inline void set_systemLabel_10(String_t* value)
	{
		___systemLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___systemLabel_10, value);
	}

	inline static int32_t get_offset_of_positionLabel_11() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___positionLabel_11)); }
	inline String_t* get_positionLabel_11() const { return ___positionLabel_11; }
	inline String_t** get_address_of_positionLabel_11() { return &___positionLabel_11; }
	inline void set_positionLabel_11(String_t* value)
	{
		___positionLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___positionLabel_11, value);
	}

	inline static int32_t get_offset_of_gpuLabel_12() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___gpuLabel_12)); }
	inline String_t* get_gpuLabel_12() const { return ___gpuLabel_12; }
	inline String_t** get_address_of_gpuLabel_12() { return &___gpuLabel_12; }
	inline void set_gpuLabel_12(String_t* value)
	{
		___gpuLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___gpuLabel_12, value);
	}

	inline static int32_t get_offset_of_rigidLabel_13() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___rigidLabel_13)); }
	inline String_t* get_rigidLabel_13() const { return ___rigidLabel_13; }
	inline String_t** get_address_of_rigidLabel_13() { return &___rigidLabel_13; }
	inline void set_rigidLabel_13(String_t* value)
	{
		___rigidLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___rigidLabel_13, value);
	}

	inline static int32_t get_offset_of_cpuLabel_14() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___cpuLabel_14)); }
	inline String_t* get_cpuLabel_14() const { return ___cpuLabel_14; }
	inline String_t** get_address_of_cpuLabel_14() { return &___cpuLabel_14; }
	inline void set_cpuLabel_14(String_t* value)
	{
		___cpuLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___cpuLabel_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___style_15)); }
	inline GUIStyle_t1006925219 * get_style_15() const { return ___style_15; }
	inline GUIStyle_t1006925219 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(GUIStyle_t1006925219 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_scoreValue_16() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___scoreValue_16)); }
	inline int32_t get_scoreValue_16() const { return ___scoreValue_16; }
	inline int32_t* get_address_of_scoreValue_16() { return &___scoreValue_16; }
	inline void set_scoreValue_16(int32_t value)
	{
		___scoreValue_16 = value;
	}

	inline static int32_t get_offset_of_highScore_17() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___highScore_17)); }
	inline int32_t get_highScore_17() const { return ___highScore_17; }
	inline int32_t* get_address_of_highScore_17() { return &___highScore_17; }
	inline void set_highScore_17(int32_t value)
	{
		___highScore_17 = value;
	}

	inline static int32_t get_offset_of_currentLives_18() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___currentLives_18)); }
	inline int32_t get_currentLives_18() const { return ___currentLives_18; }
	inline int32_t* get_address_of_currentLives_18() { return &___currentLives_18; }
	inline void set_currentLives_18(int32_t value)
	{
		___currentLives_18 = value;
	}

	inline static int32_t get_offset_of_lastUpdate_19() { return static_cast<int32_t>(offsetof(PlayerController_t2866526589, ___lastUpdate_19)); }
	inline float get_lastUpdate_19() const { return ___lastUpdate_19; }
	inline float* get_address_of_lastUpdate_19() { return &___lastUpdate_19; }
	inline void set_lastUpdate_19(float value)
	{
		___lastUpdate_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
