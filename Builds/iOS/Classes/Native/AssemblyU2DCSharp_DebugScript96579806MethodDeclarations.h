﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugScript
struct DebugScript_t96579806;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void DebugScript::.ctor()
extern "C"  void DebugScript__ctor_m2348803149 (DebugScript_t96579806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DebugScript::Start()
extern "C"  Il2CppObject * DebugScript_Start_m4045735829 (DebugScript_t96579806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugScript::OnGUI()
extern "C"  void DebugScript_OnGUI_m1844201799 (DebugScript_t96579806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
