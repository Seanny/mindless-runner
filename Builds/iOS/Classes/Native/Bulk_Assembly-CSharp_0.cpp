﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// DeathScene
struct DeathScene_t1834932888;
// DebugScript
struct DebugScript_t96579806;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// DebugScript/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4138896567;
// System.Object
struct Il2CppObject;
// Help
struct Help_t2245473;
// MenuCharacter
struct MenuCharacter_t990023786;
// MenuScript
struct MenuScript_t1549527850;
// PlatformDestruction
struct PlatformDestruction_t690538015;
// PlatformGenerator
struct PlatformGenerator_t4213597056;
// PlayerController
struct PlayerController_t2866526589;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// UnityEngine.Animation
struct Animation_t350396337;
// UnityEngine.Collision
struct Collision_t1119538015;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_DeathScene1834932888.h"
#include "AssemblyU2DCSharp_DeathScene1834932888MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3733964924MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "AssemblyU2DCSharp_DebugScript96579806.h"
#include "AssemblyU2DCSharp_DebugScript96579806MethodDeclarations.h"
#include "AssemblyU2DCSharp_DebugScript_U3CStartU3Ec__Iterat4138896567MethodDeclarations.h"
#include "AssemblyU2DCSharp_DebugScript_U3CStartU3Ec__Iterat4138896567.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_Help2245473.h"
#include "AssemblyU2DCSharp_Help2245473MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "AssemblyU2DCSharp_MenuCharacter990023786.h"
#include "AssemblyU2DCSharp_MenuCharacter990023786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "AssemblyU2DCSharp_MenuScript1549527850.h"
#include "AssemblyU2DCSharp_MenuScript1549527850MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlatformDestruction690538015.h"
#include "AssemblyU2DCSharp_PlatformDestruction690538015MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharp_PlatformGenerator4213597056.h"
#include "AssemblyU2DCSharp_PlatformGenerator4213597056MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_PlayerController2866526589.h"
#include "AssemblyU2DCSharp_PlayerController2866526589MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo4158905322MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Animation350396337.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Collision1119538015MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation350396337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, method) ((  Rigidbody_t1972007546 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, method) ((  Animation_t350396337 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DeathScene::.ctor()
extern "C"  void DeathScene__ctor_m3994888451 (DeathScene_t1834932888 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeathScene::Start()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1457329305;
extern Il2CppCodeGenString* _stringLiteral109264530;
extern Il2CppCodeGenString* _stringLiteral3588653336;
extern Il2CppCodeGenString* _stringLiteral909393168;
extern const uint32_t DeathScene_Start_m2942026243_MetadataUsageId;
extern "C"  void DeathScene_Start_m2942026243 (DeathScene_t1834932888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeathScene_Start_m2942026243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1457329305, /*hidden argument*/NULL);
		__this->set_score_2(L_0);
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral109264530, /*hidden argument*/NULL);
		__this->set_highScore_3(L_1);
		Text_t3286458198 * L_2 = __this->get_scoreText_4();
		ObjectU5BU5D_t11523773* L_3 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral3588653336);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3588653336);
		ObjectU5BU5D_t11523773* L_4 = L_3;
		int32_t L_5 = __this->get_score_2();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral909393168);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral909393168);
		ObjectU5BU5D_t11523773* L_9 = L_8;
		int32_t L_10 = __this->get_highScore_3();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3016520001(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_13);
		return;
	}
}
// System.Void DeathScene::startButton()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral573333903;
extern Il2CppCodeGenString* _stringLiteral1550783935;
extern const uint32_t DeathScene_startButton_m1960234869_MetadataUsageId;
extern "C"  void DeathScene_startButton_m1960234869 (DeathScene_t1834932888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeathScene_startButton_m1960234869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral573333903, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral1550783935, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeathScene::menuButton()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral983593288;
extern Il2CppCodeGenString* _stringLiteral1693597542;
extern const uint32_t DeathScene_menuButton_m466594034_MetadataUsageId;
extern "C"  void DeathScene_menuButton_m466594034 (DeathScene_t1834932888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeathScene_menuButton_m466594034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral983593288, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral1693597542, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugScript::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugScript__ctor_m2348803149_MetadataUsageId;
extern "C"  void DebugScript__ctor_m2348803149 (DebugScript_t96579806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugScript__ctor_m2348803149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_label_2(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator DebugScript::Start()
extern TypeInfo* U3CStartU3Ec__Iterator0_t4138896567_il2cpp_TypeInfo_var;
extern const uint32_t DebugScript_Start_m4045735829_MetadataUsageId;
extern "C"  Il2CppObject * DebugScript_Start_m4045735829 (DebugScript_t96579806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugScript_Start_m4045735829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator0_t4138896567 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t4138896567 * L_0 = (U3CStartU3Ec__Iterator0_t4138896567 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t4138896567_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m2433474597(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t4138896567 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CStartU3Ec__Iterator0_t4138896567 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DebugScript::OnGUI()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern const uint32_t DebugScript_OnGUI_m1844201799_MetadataUsageId;
extern "C"  void DebugScript_OnGUI_m1844201799 (DebugScript_t96579806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugScript_OnGUI_m1844201799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m351497798(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		Color_t1588175760  L_1 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Rect_t1525428817  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (5.0f), (30.0f), (100.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_label_2();
		GUI_Label_m1483857617(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void DebugScript/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m2433474597 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object DebugScript/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m879583309 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object DebugScript/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3506150881 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean DebugScript/<Start>c__Iterator0::MoveNext()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral67111343;
extern Il2CppCodeGenString* _stringLiteral76887510;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m2311655343_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2311655343 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m2311655343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0060;
		}
		if (L_1 == 2)
		{
			goto IL_00d1;
		}
	}
	{
		goto IL_00dd;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		bool L_2 = Debug_get_isDebugBuild_m351497798(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_depth_m4181267379(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
	}

IL_0035:
	{
		float L_3 = Time_get_timeScale_m1970669766(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_3) == ((float)(1.0f)))))
		{
			goto IL_00a5;
		}
	}
	{
		WaitForSeconds_t1291133240 * L_4 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_00df;
	}

IL_0060:
	{
		DebugScript_t96579806 * L_5 = __this->get_U3CU3Ef__this_2();
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_count_3(((float)((float)(1.0f)/(float)L_6)));
		DebugScript_t96579806 * L_7 = __this->get_U3CU3Ef__this_2();
		DebugScript_t96579806 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		float L_9 = L_8->get_count_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_10 = bankers_roundf(L_9);
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral67111343, L_12, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_label_2(L_13);
		goto IL_00b5;
	}

IL_00a5:
	{
		DebugScript_t96579806 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		L_14->set_label_2(_stringLiteral76887510);
	}

IL_00b5:
	{
		WaitForSeconds_t1291133240 * L_15 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_15, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_15);
		__this->set_U24PC_0(2);
		goto IL_00df;
	}

IL_00d1:
	{
		goto IL_0035;
	}

IL_00d6:
	{
		__this->set_U24PC_0((-1));
	}

IL_00dd:
	{
		return (bool)0;
	}

IL_00df:
	{
		return (bool)1;
	}
	// Dead block : IL_00e1: ldloc.1
}
// System.Void DebugScript/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2005161762 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void DebugScript/<Start>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m79907538_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m79907538 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m79907538_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Help::.ctor()
extern "C"  void Help__ctor_m3423372378 (Help_t2245473 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Help::Start()
extern Il2CppCodeGenString* _stringLiteral3908026166;
extern Il2CppCodeGenString* _stringLiteral4144353085;
extern Il2CppCodeGenString* _stringLiteral2335662204;
extern const uint32_t Help_Start_m2370510170_MetadataUsageId;
extern "C"  void Help_Start_m2370510170 (Help_t2245473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Help_Start_m2370510170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_002c;
		}
	}

IL_0017:
	{
		Text_t3286458198 * L_2 = __this->get_helpText_2();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral3908026166);
		goto IL_0096;
	}

IL_002c:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)31)))))
		{
			goto IL_004d;
		}
	}
	{
		Text_t3286458198 * L_4 = __this->get_helpText_2();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral3908026166);
		goto IL_0096;
	}

IL_004d:
	{
		int32_t L_5 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)18))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)((int32_t)19))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_7 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0086;
		}
	}

IL_0071:
	{
		Text_t3286458198 * L_8 = __this->get_helpText_2();
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteral4144353085);
		goto IL_0096;
	}

IL_0086:
	{
		Text_t3286458198 * L_9 = __this->get_helpText_2();
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, _stringLiteral2335662204);
	}

IL_0096:
	{
		return;
	}
}
// System.Void Help::Update()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109264530;
extern Il2CppCodeGenString* _stringLiteral1457329305;
extern Il2CppCodeGenString* _stringLiteral1451049742;
extern Il2CppCodeGenString* _stringLiteral3588653336;
extern Il2CppCodeGenString* _stringLiteral107302398;
extern Il2CppCodeGenString* _stringLiteral3299764873;
extern const uint32_t Help_Update_m477223411_MetadataUsageId;
extern "C"  void Help_Update_m477223411 (Help_t2245473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Help_Update_m477223411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral109264530, /*hidden argument*/NULL);
		__this->set_highScore_4(L_0);
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1457329305, /*hidden argument*/NULL);
		__this->set_currentScore_5(L_1);
		int32_t L_2 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1451049742, /*hidden argument*/NULL);
		__this->set_currentLives_6(L_2);
		Text_t3286458198 * L_3 = __this->get_scoreText_3();
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral3588653336);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3588653336);
		ObjectU5BU5D_t11523773* L_5 = L_4;
		int32_t L_6 = __this->get_currentScore_5();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t11523773* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral107302398);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral107302398);
		ObjectU5BU5D_t11523773* L_10 = L_9;
		int32_t L_11 = __this->get_highScore_4();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 4);
		ArrayElementTypeCheck (L_14, _stringLiteral3299764873);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3299764873);
		ObjectU5BU5D_t11523773* L_15 = L_14;
		int32_t L_16 = __this->get_currentLives_6();
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 5);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3016520001(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_19);
		return;
	}
}
// System.Void MenuCharacter::.ctor()
extern "C"  void MenuCharacter__ctor_m1677895745 (MenuCharacter_t990023786 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuCharacter::Update()
extern "C"  void MenuCharacter_Update_m2202022636 (MenuCharacter_t990023786 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Rigidbody_t1972007546 * L_0 = __this->get_myRigidbody_2();
		Rigidbody_t1972007546 * L_1 = __this->get_myRigidbody_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Rigidbody_get_velocity_m2696244068(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_y_2();
		Vector2_t3525329788  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, (5.0f), L_3, /*hidden argument*/NULL);
		Vector3_t3525329789  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody_set_velocity_m799562119(L_0, L_5, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_6 = __this->get_myRigidbody_2();
		NullCheck(L_6);
		Transform_t284553113 * L_7 = Component_get_transform_m4257140443(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_x_1();
		if ((!(((float)L_9) > ((float)(15.0f)))))
		{
			goto IL_008b;
		}
	}
	{
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		(&V_0)->set_x_1((-15.0f));
		(&V_0)->set_y_2((-2.05f));
		(&V_0)->set_z_3((0.0f));
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_13 = V_0;
		NullCheck(L_12);
		Transform_set_position_m3111394108(L_12, L_13, /*hidden argument*/NULL);
	}

IL_008b:
	{
		return;
	}
}
// System.Void MenuScript::.ctor()
extern "C"  void MenuScript__ctor_m1957993649 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::menuButton()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1786758351;
extern Il2CppCodeGenString* _stringLiteral1693597542;
extern const uint32_t MenuScript_menuButton_m2178109956_MetadataUsageId;
extern "C"  void MenuScript_menuButton_m2178109956 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_menuButton_m2178109956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1786758351, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral1693597542, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::startButton()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral640786780;
extern Il2CppCodeGenString* _stringLiteral1550783935;
extern const uint32_t MenuScript_startButton_m3477620899_MetadataUsageId;
extern "C"  void MenuScript_startButton_m3477620899 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_startButton_m3477620899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral640786780, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral1550783935, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::optionButton()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2154995949;
extern Il2CppCodeGenString* _stringLiteral3045492382;
extern const uint32_t MenuScript_optionButton_m1745396378_MetadataUsageId;
extern "C"  void MenuScript_optionButton_m1745396378 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_optionButton_m1745396378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2154995949, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral3045492382, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::exitButton()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1827672800;
extern const uint32_t MenuScript_exitButton_m4153132739_MetadataUsageId;
extern "C"  void MenuScript_exitButton_m4153132739 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_exitButton_m4153132739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1827672800, /*hidden argument*/NULL);
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::goSlow()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1397902737;
extern Il2CppCodeGenString* _stringLiteral1057263350;
extern const uint32_t MenuScript_goSlow_m2236765852_MetadataUsageId;
extern "C"  void MenuScript_goSlow_m2236765852 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_goSlow_m2236765852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1397902737, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral1057263350, 3, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::goNormal()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2865735659;
extern Il2CppCodeGenString* _stringLiteral1057263350;
extern const uint32_t MenuScript_goNormal_m370178434_MetadataUsageId;
extern "C"  void MenuScript_goNormal_m370178434 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_goNormal_m370178434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2865735659, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral1057263350, 5, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScript::goFast()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2433940822;
extern Il2CppCodeGenString* _stringLiteral1057263350;
extern const uint32_t MenuScript_goFast_m1854544439_MetadataUsageId;
extern "C"  void MenuScript_goFast_m1854544439 (MenuScript_t1549527850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuScript_goFast_m1854544439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2433940822, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral1057263350, 7, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformDestruction::.ctor()
extern "C"  void PlatformDestruction__ctor_m2824388652 (PlatformDestruction_t690538015 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformDestruction::Start()
extern Il2CppCodeGenString* _stringLiteral1759812273;
extern const uint32_t PlatformDestruction_Start_m1771526444_MetadataUsageId;
extern "C"  void PlatformDestruction_Start_m1771526444 (PlatformDestruction_t690538015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlatformDestruction_Start_m1771526444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1759812273, /*hidden argument*/NULL);
		__this->set_platformDescructionPoint_2(L_0);
		return;
	}
}
// System.Void PlatformDestruction::Update()
extern "C"  void PlatformDestruction_Update_m3383564385 (PlatformDestruction_t690538015 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		GameObject_t4012695102 * L_3 = __this->get_platformDescructionPoint_2();
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_x_1();
		if ((!(((float)L_2) < ((float)L_6))))
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void PlatformGenerator::.ctor()
extern "C"  void PlatformGenerator__ctor_m1390754283 (PlatformGenerator_t4213597056 * __this, const MethodInfo* method)
{
	{
		__this->set_platformWidth_5((37.0f));
		__this->set_platformWidth2_6((14.5f));
		__this->set_oldPlatform_7((-1));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformGenerator::Start()
extern Il2CppCodeGenString* _stringLiteral1008556333;
extern Il2CppCodeGenString* _stringLiteral315405391;
extern const uint32_t PlatformGenerator_Start_m337892075_MetadataUsageId;
extern "C"  void PlatformGenerator_Start_m337892075 (PlatformGenerator_t4213597056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlatformGenerator_Start_m337892075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_oldPlatform_7((-1));
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1008556333, /*hidden argument*/NULL);
		__this->set_thePlatform_2(L_0);
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral315405391, /*hidden argument*/NULL);
		__this->set_anotherPlatform_3(L_1);
		return;
	}
}
// System.Void PlatformGenerator::Update()
extern "C"  void PlatformGenerator_Update_m1890571906 (PlatformGenerator_t4213597056 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3525329789  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3525329789  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3525329789  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3525329789  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3525329789  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_4 = L_1;
		float L_2 = (&V_4)->get_x_1();
		Transform_t284553113 * L_3 = __this->get_generationPoint_4();
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		V_5 = L_4;
		float L_5 = (&V_5)->get_x_1();
		if ((!(((float)L_2) < ((float)L_5))))
		{
			goto IL_0188;
		}
	}
	{
		int32_t L_6 = Random_Range_m75452833(NULL /*static, unused*/, 0, 5, /*hidden argument*/NULL);
		V_0 = (((float)((float)L_6)));
		V_1 = 1;
		int32_t L_7 = V_1;
		if (L_7)
		{
			goto IL_00cc;
		}
	}
	{
		int32_t L_8 = Random_Range_m75452833(NULL /*static, unused*/, 0, 5, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_8)));
		__this->set_oldPlatform_7(0);
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_6 = L_11;
		float L_12 = (&V_6)->get_x_1();
		float L_13 = __this->get_platformWidth2_6();
		float L_14 = V_2;
		Transform_t284553113 * L_15 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3525329789  L_16 = Transform_get_position_m2211398607(L_15, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = (&V_7)->get_y_2();
		float L_18 = V_0;
		Transform_t284553113 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t3525329789  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = (&V_8)->get_z_3();
		Vector3_t3525329789  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2926210380(&L_22, ((float)((float)((float)((float)((float)((float)L_12+(float)L_13))+(float)L_14))-(float)(1.0f))), ((float)((float)L_17+(float)L_18)), L_21, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_m3111394108(L_9, L_22, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_23 = __this->get_anotherPlatform_3();
		Transform_t284553113 * L_24 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t3525329789  L_25 = Transform_get_position_m2211398607(L_24, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_26 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_23, L_25, L_26, /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_00cc:
	{
		int32_t L_27 = Random_Range_m75452833(NULL /*static, unused*/, ((int32_t)20), ((int32_t)25), /*hidden argument*/NULL);
		V_3 = (((float)((float)L_27)));
		int32_t L_28 = __this->get_oldPlatform_7();
		if (!L_28)
		{
			goto IL_00ea;
		}
	}
	{
		float L_29 = V_3;
		V_3 = ((float)((float)L_29-(float)(5.0f)));
	}

IL_00ea:
	{
		float L_30 = V_0;
		if ((!(((float)L_30) > ((float)(0.0f)))))
		{
			goto IL_00fd;
		}
	}
	{
		float L_31 = V_3;
		V_3 = ((float)((float)L_31-(float)(1.0f)));
	}

IL_00fd:
	{
		float L_32 = V_3;
		if ((!(((float)L_32) > ((float)(23.0f)))))
		{
			goto IL_0110;
		}
	}
	{
		float L_33 = V_3;
		V_3 = ((float)((float)L_33-(float)(3.0f)));
	}

IL_0110:
	{
		__this->set_oldPlatform_7(1);
		Transform_t284553113 * L_34 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_35 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t3525329789  L_36 = Transform_get_position_m2211398607(L_35, /*hidden argument*/NULL);
		V_9 = L_36;
		float L_37 = (&V_9)->get_x_1();
		float L_38 = __this->get_platformWidth_5();
		Transform_t284553113 * L_39 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t3525329789  L_40 = Transform_get_position_m2211398607(L_39, /*hidden argument*/NULL);
		V_10 = L_40;
		float L_41 = (&V_10)->get_y_2();
		float L_42 = V_0;
		Transform_t284553113 * L_43 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t3525329789  L_44 = Transform_get_position_m2211398607(L_43, /*hidden argument*/NULL);
		V_11 = L_44;
		float L_45 = (&V_11)->get_z_3();
		Vector3_t3525329789  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Vector3__ctor_m2926210380(&L_46, ((float)((float)L_37+(float)L_38)), ((float)((float)L_41+(float)L_42)), L_45, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_position_m3111394108(L_34, L_46, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_47 = __this->get_thePlatform_2();
		Transform_t284553113 * L_48 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t3525329789  L_49 = Transform_get_position_m2211398607(L_48, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_50 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_47, L_49, L_50, /*hidden argument*/NULL);
	}

IL_0188:
	{
		return;
	}
}
// System.Void PlayerController::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayerController__ctor_m2658519486_MetadataUsageId;
extern "C"  void PlayerController__ctor_m2658519486 (PlayerController_t2866526589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerController__ctor_m2658519486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_moveSpeed_2((5.0f));
		__this->set_jumpForce_3((10.0f));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_systemLabel_10(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_positionLabel_11(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_gpuLabel_12(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_rigidLabel_13(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_cpuLabel_14(L_4);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3875382237;
extern Il2CppCodeGenString* _stringLiteral2435082;
extern Il2CppCodeGenString* _stringLiteral1661943163;
extern Il2CppCodeGenString* _stringLiteral2485;
extern Il2CppCodeGenString* _stringLiteral68036786;
extern Il2CppCodeGenString* _stringLiteral185443404;
extern Il2CppCodeGenString* _stringLiteral64342702;
extern Il2CppCodeGenString* _stringLiteral3732500403;
extern Il2CppCodeGenString* _stringLiteral1082055271;
extern Il2CppCodeGenString* _stringLiteral76351;
extern Il2CppCodeGenString* _stringLiteral109264530;
extern Il2CppCodeGenString* _stringLiteral2239646549;
extern const uint32_t PlayerController_Start_m1605657278_MetadataUsageId;
extern "C"  void PlayerController_Start_m1605657278 (PlayerController_t2866526589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Start_m1605657278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = SystemInfo_get_operatingSystem_m2538828082(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3875382237, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_2 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		__this->set_myRigidbody_4(L_2);
		ObjectU5BU5D_t11523773* L_3 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral2435082);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2435082);
		ObjectU5BU5D_t11523773* L_4 = L_3;
		String_t* L_5 = SystemInfo_get_operatingSystem_m2538828082(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t11523773* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1661943163);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1661943163);
		ObjectU5BU5D_t11523773* L_7 = L_6;
		int32_t L_8 = SystemInfo_get_systemMemorySize_m114183438(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		ObjectU5BU5D_t11523773* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 4);
		ArrayElementTypeCheck (L_11, _stringLiteral2485);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2485);
		String_t* L_12 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->set_systemLabel_10(L_12);
		String_t* L_13 = SystemInfo_get_graphicsDeviceName_m4186183660(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_14 = SystemInfo_get_graphicsDeviceVendor_m3175628649(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral68036786, L_13, _stringLiteral185443404, L_14, /*hidden argument*/NULL);
		__this->set_gpuLabel_12(L_15);
		ObjectU5BU5D_t11523773* L_16 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		ArrayElementTypeCheck (L_16, _stringLiteral64342702);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral64342702);
		ObjectU5BU5D_t11523773* L_17 = L_16;
		String_t* L_18 = SystemInfo_get_processorType_m3719165582(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_18);
		ObjectU5BU5D_t11523773* L_19 = L_17;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, _stringLiteral3732500403);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3732500403);
		ObjectU5BU5D_t11523773* L_20 = L_19;
		int32_t L_21 = SystemInfo_get_processorCount_m3548598394(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_23);
		ObjectU5BU5D_t11523773* L_24 = L_20;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 4);
		ArrayElementTypeCheck (L_24, _stringLiteral1082055271);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1082055271);
		ObjectU5BU5D_t11523773* L_25 = L_24;
		int32_t L_26 = SystemInfo_get_processorFrequency_m4159864967(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 5);
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_28);
		ObjectU5BU5D_t11523773* L_29 = L_25;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 6);
		ArrayElementTypeCheck (L_29, _stringLiteral76351);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral76351);
		String_t* L_30 = String_Concat_m3016520001(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		__this->set_cpuLabel_14(L_30);
		Animation_t350396337 * L_31 = Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var);
		__this->set__anim_8(L_31);
		int32_t L_32 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral109264530, /*hidden argument*/NULL);
		__this->set_highScore_17(L_32);
		float L_33 = __this->get_moveSpeed_2();
		if ((!(((float)L_33) < ((float)(1.0f)))))
		{
			goto IL_0107;
		}
	}
	{
		__this->set_moveSpeed_2((5.0f));
	}

IL_0107:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2239646549, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::OnCollisionEnter(UnityEngine.Collision)
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2141373863;
extern Il2CppCodeGenString* _stringLiteral1769793143;
extern Il2CppCodeGenString* _stringLiteral32755469;
extern Il2CppCodeGenString* _stringLiteral734964875;
extern Il2CppCodeGenString* _stringLiteral113291;
extern const uint32_t PlayerController_OnCollisionEnter_m387453836_MetadataUsageId;
extern "C"  void PlayerController_OnCollisionEnter_m387453836 (PlayerController_t2866526589 * __this, Collision_t1119538015 * ___collision, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_OnCollisionEnter_m387453836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint_t2951122365  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ContactPointU5BU5D_t1988025008* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Collision_t1119538015 * L_0 = ___collision;
		NullCheck(L_0);
		ContactPointU5BU5D_t1988025008* L_1 = Collision_get_contacts_m658316947(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_00eb;
	}

IL_000e:
	{
		ContactPointU5BU5D_t1988025008* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		V_0 = (*(ContactPoint_t2951122365 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
		Collision_t1119538015 * L_4 = ___collision;
		NullCheck(L_4);
		Collider_t955670625 * L_5 = Collision_get_collider_m1325344374(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = GameObject_get_layer_m1648550306(L_6, /*hidden argument*/NULL);
		int32_t L_8 = LayerMask_NameToLayer_m170005213(NULL /*static, unused*/, _stringLiteral2141373863, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_009e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1769793143, /*hidden argument*/NULL);
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3525329789  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		Transform_t284553113 * L_11 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3525329789  L_12 = Transform_get_position_m2211398607(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		float L_13 = (&V_4)->get_x_1();
		(&V_3)->set_x_1(L_13);
		(&V_3)->set_y_2((-2.0f));
		Transform_t284553113 * L_14 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t3525329789  L_15 = Transform_get_position_m2211398607(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_z_3();
		(&V_3)->set_z_3(L_16);
		Transform_t284553113 * L_17 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_18 = V_3;
		NullCheck(L_17);
		Transform_set_position_m3111394108(L_17, L_18, /*hidden argument*/NULL);
	}

IL_009e:
	{
		Collider_t955670625 * L_19 = ContactPoint_get_thisCollider_m3472658310((&V_0), /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m3709440845(L_19, /*hidden argument*/NULL);
		Collider_t955670625 * L_21 = ContactPoint_get_otherCollider_m1221222194((&V_0), /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m3709440845(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1825781833(NULL /*static, unused*/, L_20, _stringLiteral32755469, L_22, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral734964875, /*hidden argument*/NULL);
		__this->set_grounded_5((bool)1);
		Animation_t350396337 * L_24 = __this->get__anim_8();
		NullCheck(L_24);
		Animation_Play_m900498501(L_24, _stringLiteral113291, /*hidden argument*/NULL);
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00eb:
	{
		int32_t L_26 = V_2;
		ContactPointU5BU5D_t1988025008* L_27 = V_1;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void PlayerController::OnCollisionExit(UnityEngine.Collision)
extern "C"  void PlayerController_OnCollisionExit_m1504342442 (PlayerController_t2866526589 * __this, Collision_t1119538015 * ___collision, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayerController::LateUpdate()
extern "C"  void PlayerController_LateUpdate_m3721258837 (PlayerController_t2866526589 * __this, const MethodInfo* method)
{
	{
		__this->set_isWaiting_7((bool)0);
		return;
	}
}
// System.Void PlayerController::Update()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2332825018;
extern Il2CppCodeGenString* _stringLiteral1457329305;
extern Il2CppCodeGenString* _stringLiteral109264530;
extern Il2CppCodeGenString* _stringLiteral86398;
extern Il2CppCodeGenString* _stringLiteral41675595;
extern Il2CppCodeGenString* _stringLiteral41676556;
extern Il2CppCodeGenString* _stringLiteral3759830110;
extern Il2CppCodeGenString* _stringLiteral9267411;
extern Il2CppCodeGenString* _stringLiteral1852801804;
extern Il2CppCodeGenString* _stringLiteral4038579545;
extern Il2CppCodeGenString* _stringLiteral3273774;
extern Il2CppCodeGenString* _stringLiteral296071536;
extern Il2CppCodeGenString* _stringLiteral3891812788;
extern Il2CppCodeGenString* _stringLiteral2458772262;
extern Il2CppCodeGenString* _stringLiteral95457908;
extern Il2CppCodeGenString* _stringLiteral1274388488;
extern const uint32_t PlayerController_Update_m2536587535_MetadataUsageId;
extern "C"  void PlayerController_Update_m2536587535 (PlayerController_t2866526589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Update_m2536587535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3525329789  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2332825018, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_0 = __this->get_myRigidbody_4();
		float L_1 = __this->get_moveSpeed_2();
		Rigidbody_t1972007546 * L_2 = __this->get_myRigidbody_4();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Rigidbody_get_velocity_m2696244068(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		float L_4 = (&V_2)->get_y_2();
		Vector2_t3525329788  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m1517109030(&L_5, L_1, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody_set_velocity_m799562119(L_0, L_6, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_scoreValue_16();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral1457329305, L_7, /*hidden argument*/NULL);
		float L_8 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = __this->get_lastUpdate_19();
		if ((!(((float)((float)((float)L_8-(float)L_9))) >= ((float)(1.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		int32_t L_10 = __this->get_scoreValue_16();
		__this->set_scoreValue_16(((int32_t)((int32_t)L_10+(int32_t)1)));
		int32_t L_11 = __this->get_scoreValue_16();
		int32_t L_12 = __this->get_highScore_17();
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_13 = __this->get_scoreValue_16();
		__this->set_highScore_17(L_13);
		int32_t L_14 = __this->get_highScore_17();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral109264530, L_14, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_009e:
	{
		float L_15 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastUpdate_19(L_15);
	}

IL_00a9:
	{
		Transform_t284553113 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t3525329789  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		ObjectU5BU5D_t11523773* L_18 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, _stringLiteral86398);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral86398);
		ObjectU5BU5D_t11523773* L_19 = L_18;
		float L_20 = (&V_0)->get_x_1();
		float L_21 = L_20;
		Il2CppObject * L_22 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = L_19;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral41675595);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral41675595);
		ObjectU5BU5D_t11523773* L_24 = L_23;
		float L_25 = (&V_0)->get_y_2();
		float L_26 = L_25;
		Il2CppObject * L_27 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
		ObjectU5BU5D_t11523773* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 4);
		ArrayElementTypeCheck (L_28, _stringLiteral41676556);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41676556);
		ObjectU5BU5D_t11523773* L_29 = L_28;
		float L_30 = (&V_0)->get_z_3();
		float L_31 = L_30;
		Il2CppObject * L_32 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 5);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m3016520001(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		__this->set_positionLabel_11(L_33);
		ObjectU5BU5D_t11523773* L_34 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		ArrayElementTypeCheck (L_34, _stringLiteral3759830110);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3759830110);
		ObjectU5BU5D_t11523773* L_35 = L_34;
		float L_36 = __this->get_moveSpeed_2();
		float L_37 = L_36;
		Il2CppObject * L_38 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 1);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t11523773* L_39 = L_35;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 2);
		ArrayElementTypeCheck (L_39, _stringLiteral9267411);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral9267411);
		ObjectU5BU5D_t11523773* L_40 = L_39;
		float L_41 = __this->get_jumpForce_3();
		float L_42 = L_41;
		Il2CppObject * L_43 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 3);
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_43);
		String_t* L_44 = String_Concat_m3016520001(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		__this->set_rigidLabel_13(L_44);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1852801804, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_45 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0169;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_46 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01c4;
		}
	}

IL_0169:
	{
		bool L_47 = __this->get_grounded_5();
		if (!L_47)
		{
			goto IL_01c4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral4038579545, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_48 = __this->get_myRigidbody_4();
		Rigidbody_t1972007546 * L_49 = __this->get_myRigidbody_4();
		NullCheck(L_49);
		Vector3_t3525329789  L_50 = Rigidbody_get_velocity_m2696244068(L_49, /*hidden argument*/NULL);
		V_3 = L_50;
		float L_51 = (&V_3)->get_x_1();
		float L_52 = __this->get_jumpForce_3();
		Vector2_t3525329788  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Vector2__ctor_m1517109030(&L_53, L_51, L_52, /*hidden argument*/NULL);
		Vector3_t3525329789  L_54 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_48);
		Rigidbody_set_velocity_m799562119(L_48, L_54, /*hidden argument*/NULL);
		Animation_t350396337 * L_55 = __this->get__anim_8();
		NullCheck(L_55);
		Animation_Play_m900498501(L_55, _stringLiteral3273774, /*hidden argument*/NULL);
		__this->set_grounded_5((bool)0);
	}

IL_01c4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral296071536, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_56 = __this->get_myRigidbody_4();
		NullCheck(L_56);
		Vector3_t3525329789  L_57 = Rigidbody_get_position_m1751901360(L_56, /*hidden argument*/NULL);
		V_4 = L_57;
		float L_58 = (&V_4)->get_y_2();
		if ((!(((float)L_58) < ((float)(-20.0f)))))
		{
			goto IL_02b4;
		}
	}
	{
		bool L_59 = __this->get_isWaiting_7();
		if (L_59)
		{
			goto IL_02b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3891812788, /*hidden argument*/NULL);
		int32_t L_60 = __this->get_highScore_17();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral109264530, L_60, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_61 = __this->get_currentLives_18();
		if ((((int32_t)L_61) >= ((int32_t)1)))
		{
			goto IL_023b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2458772262, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral95457908, /*hidden argument*/NULL);
		goto IL_02b4;
	}

IL_023b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1274388488, /*hidden argument*/NULL);
		Transform_t284553113 * L_62 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Vector3_t3525329789  L_63 = Transform_get_position_m2211398607(L_62, /*hidden argument*/NULL);
		V_1 = L_63;
		Transform_t284553113 * L_64 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t3525329789  L_65 = Transform_get_position_m2211398607(L_64, /*hidden argument*/NULL);
		V_5 = L_65;
		float L_66 = (&V_5)->get_x_1();
		(&V_1)->set_x_1(L_66);
		Transform_t284553113 * L_67 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t3525329789  L_68 = Transform_get_position_m2211398607(L_67, /*hidden argument*/NULL);
		V_6 = L_68;
		float L_69 = (&V_6)->get_y_2();
		(&V_1)->set_y_2(((float)((float)L_69+(float)(20.0f))));
		Transform_t284553113 * L_70 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_70);
		Vector3_t3525329789  L_71 = Transform_get_position_m2211398607(L_70, /*hidden argument*/NULL);
		V_7 = L_71;
		float L_72 = (&V_7)->get_z_3();
		(&V_1)->set_z_3(L_72);
		Transform_t284553113 * L_73 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_74 = V_1;
		NullCheck(L_73);
		Transform_set_position_m3111394108(L_73, L_74, /*hidden argument*/NULL);
	}

IL_02b4:
	{
		return;
	}
}
// System.Void PlayerController::OnGUI()
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t PlayerController_OnGUI_m2153918136_MetadataUsageId;
extern "C"  void PlayerController_OnGUI_m2153918136 (PlayerController_t2866526589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_OnGUI_m2153918136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t1588175760  L_0 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		bool L_1 = Debug_get_isDebugBuild_m351497798(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00c8;
		}
	}
	{
		Rect_t1525428817  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (5.0f), (45.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_systemLabel_10();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Rect_t1525428817  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m3291325233(&L_4, (5.0f), (60.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_5 = __this->get_positionLabel_11();
		GUI_Label_m1483857617(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Rect_t1525428817  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (5.0f), (75.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_7 = __this->get_gpuLabel_12();
		GUI_Label_m1483857617(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Rect_t1525428817  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m3291325233(&L_8, (5.0f), (90.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_9 = __this->get_rigidLabel_13();
		GUI_Label_m1483857617(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Rect_t1525428817  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Rect__ctor_m3291325233(&L_10, (5.0f), (105.0f), (600.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_11 = __this->get_cpuLabel_14();
		GUI_Label_m1483857617(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
