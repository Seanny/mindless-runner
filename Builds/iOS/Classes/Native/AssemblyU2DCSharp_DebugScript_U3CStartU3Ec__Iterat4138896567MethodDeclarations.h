﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugScript/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4138896567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DebugScript/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m2433474597 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DebugScript/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m879583309 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DebugScript/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3506150881 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DebugScript/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2311655343 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugScript/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2005161762 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugScript/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m79907538 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
