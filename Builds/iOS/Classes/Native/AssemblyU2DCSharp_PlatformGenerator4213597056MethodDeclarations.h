﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformGenerator
struct PlatformGenerator_t4213597056;

#include "codegen/il2cpp-codegen.h"

// System.Void PlatformGenerator::.ctor()
extern "C"  void PlatformGenerator__ctor_m1390754283 (PlatformGenerator_t4213597056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformGenerator::Start()
extern "C"  void PlatformGenerator_Start_m337892075 (PlatformGenerator_t4213597056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformGenerator::Update()
extern "C"  void PlatformGenerator_Update_m1890571906 (PlatformGenerator_t4213597056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
