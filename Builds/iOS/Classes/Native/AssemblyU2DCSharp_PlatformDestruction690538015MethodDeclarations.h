﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformDestruction
struct PlatformDestruction_t690538015;

#include "codegen/il2cpp-codegen.h"

// System.Void PlatformDestruction::.ctor()
extern "C"  void PlatformDestruction__ctor_m2824388652 (PlatformDestruction_t690538015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformDestruction::Start()
extern "C"  void PlatformDestruction_Start_m1771526444 (PlatformDestruction_t690538015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformDestruction::Update()
extern "C"  void PlatformDestruction_Update_m3383564385 (PlatformDestruction_t690538015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
