﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuCharacter
struct MenuCharacter_t990023786;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuCharacter::.ctor()
extern "C"  void MenuCharacter__ctor_m1677895745 (MenuCharacter_t990023786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuCharacter::Update()
extern "C"  void MenuCharacter_Update_m2202022636 (MenuCharacter_t990023786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
