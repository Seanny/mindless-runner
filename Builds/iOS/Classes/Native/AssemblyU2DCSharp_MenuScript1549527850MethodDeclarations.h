﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuScript
struct MenuScript_t1549527850;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuScript::.ctor()
extern "C"  void MenuScript__ctor_m1957993649 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::menuButton()
extern "C"  void MenuScript_menuButton_m2178109956 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::startButton()
extern "C"  void MenuScript_startButton_m3477620899 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::optionButton()
extern "C"  void MenuScript_optionButton_m1745396378 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::exitButton()
extern "C"  void MenuScript_exitButton_m4153132739 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::goSlow()
extern "C"  void MenuScript_goSlow_m2236765852 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::goNormal()
extern "C"  void MenuScript_goNormal_m370178434 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuScript::goFast()
extern "C"  void MenuScript_goFast_m1854544439 (MenuScript_t1549527850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
