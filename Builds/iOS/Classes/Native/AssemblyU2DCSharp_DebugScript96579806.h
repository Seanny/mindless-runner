﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugScript
struct  DebugScript_t96579806  : public MonoBehaviour_t3012272455
{
public:
	// System.String DebugScript::label
	String_t* ___label_2;
	// System.Single DebugScript::count
	float ___count_3;

public:
	inline static int32_t get_offset_of_label_2() { return static_cast<int32_t>(offsetof(DebugScript_t96579806, ___label_2)); }
	inline String_t* get_label_2() const { return ___label_2; }
	inline String_t** get_address_of_label_2() { return &___label_2; }
	inline void set_label_2(String_t* value)
	{
		___label_2 = value;
		Il2CppCodeGenWriteBarrier(&___label_2, value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(DebugScript_t96579806, ___count_3)); }
	inline float get_count_3() const { return ___count_3; }
	inline float* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(float value)
	{
		___count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
