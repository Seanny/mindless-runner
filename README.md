# Mindless Runner

Endless Runner platformer game created using Unity3D for Mobile Platforms

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

All Platforms:
Install Unity3D
Install Android SDK

OS X:
Install XCode

Windows:
Install Visual Studio

### Prerequisities

What things you need to install the software and how to install them

Unity3D
Git
XCode (for iOS Deployment)
Android SDK (for Android Deployment)
Visual Studio (for Windows Store Deployment)

## Deployment

Use XCode for iOS
Use Android SDK for Android
Use Visual Studio for Windows Store

## Built With

* Unity3D
* MonoDevelop on OS X, Visual Studio on Windows.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Sean McElholm** - *Initial work*

## License

~~This project is licensed under Mindless Runner EULA.~~
This is now licensed under the MIT license, see LICENSE.md.

## Acknowledgments

* Sean McElholm (Project Lead)