﻿using UnityEngine;
using System.Collections;

public class MenuCharacter : MonoBehaviour {

	public Rigidbody myRigidbody;
	
	// Update is called once per frame
	void Update () {
		myRigidbody.velocity = new Vector2(5, myRigidbody.velocity.y);

		if (myRigidbody.transform.position.x > 15) {
			Vector3 pos = transform.position;
			pos.x = -15;
			pos.y = -2.05f;
			pos.z = 0.0f;
			transform.position = pos;
		}
	}
}
