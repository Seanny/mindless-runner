/*
 * Copyright (C) 2016 Sean McElholm
 */

﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	/*
	 * TODO: Add a score system that generates points based on how far they have been running.
	 */
	/*public float moveSpeed = 5;
	public float jumpForce = 10;*/
	public float moveSpeed = 5;
	public float jumpForce = 10;



	private Rigidbody myRigidbody;

	public bool grounded;
	public LayerMask whatIsGround;

	public bool isWaiting;

	private Animation _anim;

	private Vector3 oldX;

	//Debugging labels
	private string systemLabel = "";
	private string positionLabel = "";
	private string gpuLabel = "";
	private string rigidLabel = "";
	private string cpuLabel = "";
//	private string deploymentLabel = "";

	//Score System
	//TODO: Count Score for every 1 seconds they are alive

	public GUIStyle style;
	//private string scoreLabel;
	public int scoreValue;
	public int highScore;
	public int currentLives

;	private float lastUpdate;

	// Use this for initialization
	void Start () {
		Debug.Log("Starting Endless Runner on " + SystemInfo.operatingSystem);
		myRigidbody = GetComponent<Rigidbody>();
		systemLabel = "OS: " + SystemInfo.operatingSystem + ", Memory: " + SystemInfo.systemMemorySize + "Mb";
		gpuLabel = "GPU: " + SystemInfo.graphicsDeviceName + ", Vender: " + SystemInfo.graphicsDeviceVendor;
		cpuLabel = "CPU: " + SystemInfo.processorType + ", Processor Count: " + SystemInfo.processorCount + ", Clock Speed: " + SystemInfo.processorFrequency + "MHz";
		_anim = this.GetComponent<Animation>();
		highScore = PlayerPrefs.GetInt("score");

		if (moveSpeed < 1) 
		{
			moveSpeed = 5;
		}

		//InvokeRepeating ("ScoreUpdate", 1, 1.0);
		//grounded = true;
		Debug.Log("Script started!");
	}

	void OnCollisionEnter(Collision collision)
	{
		/*if (collision.gameObject.tag == "Ground") {
			Debug.Log("grounded = true");
			grounded = true;
			_anim.Play("run");
		}*/

		foreach (ContactPoint contact in collision.contacts)
		{
			if (collision.collider.gameObject.layer != LayerMask.NameToLayer("Ground"))
			{
				Debug.Log("Not touching ground.");
				Vector3 pos = transform.position;
				pos.x = transform.position.x;
				pos.y = -2;
				pos.z = transform.position.z;
				transform.position = pos;

			}
            print(contact.thisCollider.name + " hit " + contact.otherCollider.name);
			//Debug.Log("grounded = true");
			print("grounded = true");
			grounded = true;
			_anim.Play("run");
        }
	}

	void OnCollisionExit(Collision collision)
	{
		/*Debug.Log("grounded = false");
		grounded = false;*/
		//_anim.Play("run");
	}

	void LateUpdate()
	{
		/*
		 * Check if the characters y axis is less than -10, if it is then return them to the start position
		 * TODO: Reset the score also!
		 */
		isWaiting = false;
	}

	// Update is called once per frame
	void Update ()
	{
		Debug.Log ("Move character");
		myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);
		PlayerPrefs.SetInt("currentScore", scoreValue);

		if (Time.time - lastUpdate >= 1f) {
			scoreValue = scoreValue + 1;
			if (scoreValue > highScore) {
				highScore = scoreValue;
				PlayerPrefs.SetInt("score", highScore);
				PlayerPrefs.Save ();
			}
			//scoreLabel = "Score: " + scoreValue + ". High Score: " + highScore;
			lastUpdate = Time.time;
		}

		/* Get the X, Y and Z Axis and update it */
		Vector3 currentPos = transform.position;
		positionLabel = "X: " + currentPos.x + ", Y: " + currentPos.y + ", Z: " + currentPos.z;
		rigidLabel = "Move Speed: " + moveSpeed + ", Jump Force: " + jumpForce;

		Debug.Log ("Check if user wants to jump");
		if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
		{
			if(grounded == true)
			{
				Debug.Log("Character is jumping.");
				myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
				_anim.Play("jump");
				grounded = false;
			}
		}

		Debug.Log ("Check if user is below -20");
		if(myRigidbody.position.y < -20f && isWaiting == false)
		{
			Debug.Log("Character is below -20, setting back to start position");


			PlayerPrefs.SetInt("score", highScore);
			PlayerPrefs.Save ();

			if (currentLives < 1) {
				Debug.Log("No lives.");
				Application.LoadLevel ("death");
			} 
			else {
				Debug.Log("Has lives left.");
				Vector3 pos = transform.position;
				pos.x = transform.position.x;
				pos.y = transform.position.y + 20;
				pos.z = transform.position.z;
				transform.position = pos;
			}
			//Application.LoadLevel ("death");
			//Application.LoadLevel ("running"); 

			/*Vector3 pos = transform.position;
			Debug.Log("setting xyz");
			pos.x = -12;
			pos.y = 1;
			pos.z = 0.0f;
			Debug.Log("transform pos");
			transform.position = pos;
			scoreValue = 0;
			isWaiting = true;*/
			//myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);
		}
	}

	void OnGUI ()
	{
		GUI.contentColor = Color.black;
		if (Debug.isDebugBuild)
		{
			GUI.Label (new Rect (5, 45, 500, 25), systemLabel);
			GUI.Label (new Rect (5, 60, 500, 25), positionLabel);
			GUI.Label (new Rect (5, 75, 500, 25), gpuLabel);
			GUI.Label (new Rect (5, 90, 500, 25), rigidLabel);
			GUI.Label (new Rect (5, 105, 600, 25), cpuLabel);
		}

		//style.font
		//GUI.Label (new Rect (5, 15, 600, 40), scoreLabel, style);
	}
}
