﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour {

	public GameObject thePlatform;
	public GameObject anotherPlatform;
	public Transform generationPoint;
	public float platformWidth = 37;
	public float platformWidth2 = 14.5f;

	private int oldPlatform = -1;

	// Use this for initialization
	void Start () 
	{
		oldPlatform = -1;
		thePlatform = GameObject.Find ("Ground_Medium");
		anotherPlatform = GameObject.Find ("Ground_Small");

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.x < generationPoint.position.x) 
		{
			float randomHeight = Random.Range(0, 5);
			int randomPlatform = 1;//Random.Range(0, 2);

			//Lets make it "slightly" easier. :)

			if (randomPlatform == 0) 
			{
				float randomLength = Random.Range(0, 5);

				oldPlatform = 0;
				transform.position = new Vector3 (transform.position.x + platformWidth2 + randomLength - 1, transform.position.y + randomHeight, transform.position.z);
				Instantiate (anotherPlatform, transform.position, Quaternion.identity);
			} 
			else 
			{
				float randomLength = Random.Range(20, 25);
				if (oldPlatform != 0) {
					randomLength = randomLength - 5;
				}

				if (randomHeight > 0) {
					randomLength = randomLength - 1;
				}

				if (randomLength > 23) 
				{
					randomLength = randomLength - 3;
				}

				oldPlatform = 1;
				transform.position = new Vector3 (transform.position.x + platformWidth, transform.position.y + randomHeight, transform.position.z);
				Instantiate (thePlatform, transform.position, Quaternion.identity);
			}
		}
	}
}
