﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

	public void menuButton()
	{
		Debug.Log ("Starting Menu...");

		//Load the main menu
		Application.LoadLevel ("Main Menu");
	}

	public void startButton()
	{
		Debug.Log ("Starting Game...");

		//Load the main scene
		Application.LoadLevel ("running");
	}

	public void optionButton()
	{
		Debug.Log ("Options Menu...");

		//Load the options menu
		Application.LoadLevel ("options");
	}

	public void exitButton()
	{
		Debug.Log ("Exiting Game...");

		//Quit the application
		Application.Quit ();
	}

	public void goSlow()
	{
		Debug.Log ("Setting Speed to Slow...");
		PlayerPrefs.SetInt("moveSpeed", 3);
		PlayerPrefs.Save ();
	}

	public void goNormal()
	{
		Debug.Log ("Setting Speed to Normal...");
		PlayerPrefs.SetInt("moveSpeed", 5);
		PlayerPrefs.Save ();
	}
		
	public void goFast()
	{
		Debug.Log ("Setting Speed to Fast...");
		PlayerPrefs.SetInt("moveSpeed", 7);
		PlayerPrefs.Save ();
	}
}
