﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Help : MonoBehaviour {

	public Text helpText;
	public Text scoreText;

	private int highScore;
	private int currentScore;
	private int currentLives;

	// Use this for initialization
	void Start () {
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			helpText.text = "Tap the SCREEN to jump!"; /* Google Android and Apple iOS Players */
		}
		else if (Application.platform == RuntimePlatform.tvOS) {
			helpText.text = "Tap the SCREEN to jump!"; /* Apple tvOS Player */
		}
		else if (Application.platform == RuntimePlatform.WSAPlayerX86 || Application.platform == RuntimePlatform.WSAPlayerX64 || Application.platform == RuntimePlatform.WSAPlayerARM) {
			helpText.text = "Tap the SCREEN or SPACE to jump!"; /* Microsoft Windows Store Players */
		}
		else {
			helpText.text = "Tap SPACE or LEFT MOUSE to jump!"; /* Desktop platforms such as Desktop Windows, OS X and Linux */
		}
	}
	
	// Update is called once per frame
	void Update () {
		highScore = PlayerPrefs.GetInt("score");
		currentScore = PlayerPrefs.GetInt("currentScore");
		currentLives = PlayerPrefs.GetInt("currentLives");
		scoreText.text = "Score: " + currentScore + " | High Score: " + highScore + " | Lives: " + currentLives;
		//Score: 99999 | High Score: 99999

	}
}
