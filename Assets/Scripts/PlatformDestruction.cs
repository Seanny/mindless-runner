﻿using UnityEngine;
using System.Collections;

public class PlatformDestruction : MonoBehaviour {

	public GameObject platformDescructionPoint;

	// Use this for initialization
	void Start () 
	{
		platformDescructionPoint = GameObject.Find ("PlatformDestructionPoint");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.x < platformDescructionPoint.transform.position.x) 
		{
			Destroy (gameObject);
		}
	}
}
