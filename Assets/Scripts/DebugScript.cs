﻿using UnityEngine;
using System.Collections;

public class DebugScript : MonoBehaviour {

	string label = "";
	float count;

	IEnumerator Start ()
	{
		if (Debug.isDebugBuild)
		{
			GUI.depth = 2;
			while (true) {
				if (Time.timeScale == 1) {
					yield return new WaitForSeconds (0.1f);
					count = (1 / Time.deltaTime);
					label = "FPS: " + (Mathf.Round (count));
				}
				else
				{
					label = "Pause";
				}
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void OnGUI ()
	{
		if (Debug.isDebugBuild)
		{
			GUI.contentColor = Color.black;
			GUI.Label (new Rect (5, 30, 100, 25), label);
		}
	}
}
