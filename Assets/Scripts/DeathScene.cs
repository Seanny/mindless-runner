﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeathScene : MonoBehaviour {

	public int score;
	public int highScore;
	public Text scoreText;

	void Start()
	{
		score = PlayerPrefs.GetInt("currentScore");
		highScore = PlayerPrefs.GetInt("score");

		scoreText.text="Score: " + score + "\n High Score: " + highScore;
	}

	public void startButton()
	{
		Debug.Log ("Restarting Game...");

		//Load the main scene
		Application.LoadLevel ("running");
	}

	public void menuButton()
	{
		Debug.Log ("Main Menu...");

		//Load the options menu
		Application.LoadLevel ("Main Menu");
	}
}
